## Version 1.0.x

*Version 1.0.0*
Initial release

*Version 1.0.1*
Split the output in batches so Unity supports printing hundreds of lines
