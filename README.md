# Animation Linter

Installs under `Assets/Tools/Lint animations`.
This will scan all prefabs in the project, and inspect all linked animators to report any broken animation key binds.

You can use this in the CI by running the following command line:

```bash
UNITY_EXECUTABLE -batchmode -quit -nographics -executemethod Linting.AnimationsLinter.LintAnimations
```

A log will be printed listing all the found animation clips, and their state (broken/ok).
An exception will be raised if any broken link  was found.
