using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEditor;
using UnityEngine;

namespace Linting
{
    public static class AnimationsLinter
    {
        private class AnimationClipReport
        {
            public readonly HashSet<GameObject> validUsages = new HashSet<GameObject>();
            public readonly HashSet<GameObject> brokenUsages = new HashSet<GameObject>();
        }

        [MenuItem("Assets/Tools/Lint animations")]
        public static void LintAnimations()
        {
            var report = new Dictionary<AnimationClip, AnimationClipReport>();

            // Animation paths depend on the prefab on which they are used. So open all prefabs, find all animators,
            // and ensure all of their animations have all their paths satisfied.
            foreach (var prefabId in AssetDatabase.FindAssets("t:prefab"))
            {
                var prefab = AssetDatabase.LoadAssetAtPath<GameObject>(AssetDatabase.GUIDToAssetPath(prefabId));
                foreach (var animator in prefab.GetComponentsInChildren<Animator>(true))
                {
                    foreach (var animationClip in AnimationUtility.GetAnimationClips(animator.gameObject))
                    {
                        if (!report.ContainsKey(animationClip))
                        {
                            report.Add(animationClip, new AnimationClipReport());
                        }

                        foreach (var floatBinding in AnimationUtility.GetCurveBindings(animationClip))
                        {
                            UpdateReportWithBinding(prefab, animator.transform, floatBinding.path, report[animationClip], floatBinding.type);
                        }

                        foreach (var objectBinding in AnimationUtility.GetObjectReferenceCurveBindings(animationClip))
                        {
                            UpdateReportWithBinding(prefab, animator.transform, objectBinding.path, report[animationClip], objectBinding.type);
                        }
                    }
                }
            }

            PrintReport(report);

            if (report.Values.Any(v => v.brokenUsages.Count > 0))
            {
                throw new Exception("Animations have broken keys. Check the report above to see which.");
            }
        }

        private static void PrintReport(Dictionary<AnimationClip,AnimationClipReport> report)
        {
            /* Using a string builder will force unity console to print everything at once, which seems to fails in case of big strings with the following error.
             * ArgumentOutOfRangeException: Index and length must refer to a location within the string.
             * Parameter name: length
             * System.String.Substring (System.Int32 startIndex, System.Int32 length) (at <695d1cc93cca45069c528c15c9fdd749>:0)
             */ 
            //  After testing, it seems that it is neither a fixed max line not a fixed character lines. I could not find an exact reason fo the error, so we will simply print a new line every 100 lines, which seems fine

            var sb = new StringBuilder();
            var nbBatchLines = 50;
            var currentBrokenBatchIndex = 1;
            var nbBrokenCache = 0;
            var brokenElements = report.Where(kvp => kvp.Value.brokenUsages.Count > 0).ToList();
            var totalBrokenBatches = Mathf.CeilToInt(brokenElements.Count / (float)nbBatchLines);
            
            sb.AppendLine("Animation references report");
            sb.AppendLine(brokenElements.Count == 0
                ? "No Broken Animation!"
                : $"Broken animations ({currentBrokenBatchIndex}/{totalBrokenBatches}):");
            currentBrokenBatchIndex++;

            foreach (var kvp in brokenElements)
            {
                sb.AppendLine($"- {kvp.Key.name} from prefab: {string.Join(", ", kvp.Value.brokenUsages.Select(s => s.name))}");
                nbBrokenCache++;

                if (nbBrokenCache % nbBatchLines == 0)
                {
                    nbBrokenCache = 0;
                    sb.Dump();
                    sb.AppendLine("Animation references report");
                    sb.AppendLine($"Broken animations ({currentBrokenBatchIndex}/{totalBrokenBatches}):");
                    currentBrokenBatchIndex++;
                }
            }
            
            sb.Dump();
            
            var currentValidBatchIndex = 1;
            var nbValidCache = 0;
            var validElements = report.Where(kvp => kvp.Value.validUsages.Count > 0).ToList();
            var totalValidBatches = Mathf.CeilToInt(validElements.Count / (float)nbBatchLines);
            sb.AppendLine("Animation references report");
            sb.AppendLine(validElements.Count == 0
                ? "No Valid Animation!"
                : $"Valid animations ({currentValidBatchIndex}/{totalValidBatches}):");
            currentValidBatchIndex++;
            foreach (var kvp in validElements)
            {
                sb.AppendLine($"- {kvp.Key.name} from prefab: {string.Join(", ", kvp.Value.validUsages.Select(s => s.name))}");
                nbValidCache++;
                    
                if (nbValidCache % nbBatchLines == 0)
                {
                    nbValidCache = 0;
                    sb.Dump();
                    sb.AppendLine("Animation references report");
                    sb.AppendLine($"Valid animations ({currentValidBatchIndex}/{totalValidBatches}):");
                    currentValidBatchIndex++;
                }
            }

            sb.Dump();
        }

        private static void Dump(this StringBuilder stringBuilder)
        {
            Debug.Log(stringBuilder.ToString());
            stringBuilder.Clear();
        }

        private static void UpdateReportWithBinding(GameObject prefabRoot, 
            Transform currentGameObject, string bindingPath, AnimationClipReport report, Type animatedPropertyType)
        {
            // An empty path targets the current GO, which always succeeds.
            if (bindingPath == "")
            {
                if (animatedPropertyType == typeof(GameObject) || currentGameObject.GetComponent(animatedPropertyType) != null)
                {
                    report.validUsages.Add(prefabRoot); 
                }
                else
                {
                    report.brokenUsages.Add(prefabRoot);
                }
                
                return;
            }
            
            var paths = bindingPath.Split('/');
            
            // Other paths target a child further down the hierarchy. First ensure the direct child exists.
            var child = GetChildNamed(currentGameObject.transform, paths[0]);
            if (child == null)
            {
                report.brokenUsages.Add(prefabRoot);
                return;
            }
            
            // Longer paths require recursion. Traverse the remainder of the path (path[1:])
            UpdateReportWithBinding(prefabRoot, child, string.Join("/", paths.Skip(1)), report, animatedPropertyType);
        }

        private static Transform GetChildNamed(Transform root, string name)
        {
            for (int i = 0; i < root.childCount; i++)
            {
                if (root.GetChild(i).name == name)
                {
                    return root.GetChild(i);
                }
            }

            return null;
        }
    }
}